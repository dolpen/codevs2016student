# codevs for student (2016)

* https://student.codevs.jp/

## 開発情報

* 言語：Java8

## パッケージ情報

```
net.dolpen.codevs2016
  ├ ai
  │  └ Brain 思考ロジックのインターフェース
  │    └ xxxBrain : IF実装入れ替えてAI挙動を変更する
  ├ models : ゲームとしての仕様/実装
  ├ sims : ゲームの観測、評価や思考のための情報
  ├ utils : ボイラープレート
  ├ Specification.java : ゲーム仕様/AIの構成/性能項目管理
  └ Main.java : ブートストラップ
```
