package net.dolpen.codevs2016.models;

import net.dolpen.codevs2016.sims.Policy;

/**
 * 盤面評価オブジェクト
 */
public class Stats {

    public boolean dead = true;

    public int chain = 0;

    public int score = 0;

    public int damage = 0;

    public int sweeped = 0;

    public int cells = 0;

    public int obstacles = 0;

    public int blocks = 0;

    public int potential = 0;

    public boolean isFired() {
        return chain > 0;
    }

    public boolean isDangerous() {
        return blocks * 10 >= cells * 9;
    }

    public boolean isDead() {
        return dead;
    }

    public int getScore(Policy policy) {
        return policy.evaluate(this);
    }

    public String toString() {
        return String.format("e=%2d,s=%2d,d=%2d,c=%2d", sweeped, score, damage, chain);
    }

}
