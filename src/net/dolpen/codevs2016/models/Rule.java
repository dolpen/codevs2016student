package net.dolpen.codevs2016.models;

import net.dolpen.codevs2016.Specification;

import java.util.Scanner;

/**
 * ゲーム1戦通して変化しない設定値
 */
public class Rule {

    public int obstacle; // おじゃま

    public int target; // 和

    public int empty; // 空白セル

    public int width; // 盤面横幅

    public int height; // 盤面高さ

    public int packSize; // パック辺長さ

    public int turns; // 総合ターン数

    public Rule(int width, int height, int packSize, int target, int turns) {
        this.width = width;
        this.height = height;
        this.packSize = packSize;
        this.target = target;
        this.obstacle = target + 1;
        this.empty = Specification.CELL_EMPTY;
        this.turns = turns;
    }

    public static Rule initFromScanner(Scanner sc) {
        int width = sc.nextInt();
        int height = sc.nextInt();
        int packSize = sc.nextInt();
        int target = sc.nextInt();
        int turns = sc.nextInt();
        return new Rule(width, height, packSize, target, turns);
    }

    public int getBoardHeight() {
        return height + packSize;
    }

    public int getBoardDeadLine() {
        return packSize;
    }

    public boolean isEmptyCell(int cellValue) {
        return cellValue == empty;
    }

    public boolean isNumberCell(int cellValue) {
        return cellValue > empty && cellValue <= target;
    }

    public boolean isBlockCell(int cellValue) {
        return cellValue != empty;
    }

    public boolean isObstacleCell(int cellValue) {
        return cellValue == obstacle;
    }


}
