package net.dolpen.codevs2016.models;

import net.dolpen.codevs2016.Specification;
import net.dolpen.codevs2016.utils.Array2d;

import java.util.*;

/**
 * 盤面
 */
public class Board {
    // マーク時の探索方向とかそういうやつです
    static int[][] d = new int[][]{
        {1, 0}, {0, 1}, {1, 1}, {-1, 1}
    };

    // お邪魔ブロックの数
    public int stocked;

    public Rule rule;

    public int cells[][];

    public Board(int[][] cells, Rule rule, int stocked) {
        this.cells = cells;
        this.rule = rule;
        this.stocked = stocked;
    }

    public Board clone() {
        return new Board(
            Array2d.clone(cells, rule.width, rule.getBoardHeight()),
            rule,
            stocked
        );
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("-----board-----\n");
        sb.append(Array2d.toString(cells, rule.width, rule.getBoardHeight()));
        sb.append("----/board-----\n");
        return sb.toString();
    }

    public static Board fromScanner(Scanner sc, Rule rule, int stocked) {
        return new Board(
            Array2d.offsetFromScanner(sc, rule.width, rule.height, rule.packSize),
            rule,
            stocked
        );
    }

    // パックを置く。falseが帰ってきたら死ぬ
    private boolean put(Pack pack, int x) { // returns successful put
        for (int j = 0; j < rule.packSize; ++j) {
            int bj = x + j;
            if (bj < 0 || bj >= rule.width) continue;
            for (int i = 0; i < pack.size; ++i) {
                if (rule.isEmptyCell(pack.cells[i][j])) continue;
                if (rule.isBlockCell(cells[i][bj])) return false;
                cells[i][bj] = pack.cells[i][j];
            }
        }
        return true;
    }

    // 重力が働く。ブロックセルは下に移動する
    private void fall() {
        int height = rule.getBoardHeight();
        for (int j = 0; j < rule.width; j++) {
            int top = -1;
            for (int i = height - 1; i >= 0; i--) {
                if (rule.isEmptyCell(cells[i][j])) {
                    top = i;
                    break;
                }
            }
            for (int i = top - 1; i >= 0; i--) {
                // num or obstacle
                if (rule.isBlockCell(cells[i][j])) {
                    cells[top][j] = cells[i][j];
                    cells[i][j] = rule.empty;
                    top--;
                }
            }
        }
    }

    // 消す対象のセルをマーキングする
    private int[][] mark() {
        int height = rule.getBoardHeight();
        int[][] ret = Array2d.zero(rule.width, height);
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < rule.width; ++j) {
                for (int k = 0; k < 4; ++k) {
                    int sum = 0;
                    int l = -1;
                    int jmin = d[k][1] == 0 ? rule.target : (rule.width - j);
                    int imin = d[k][0] == 0 ? rule.target : (d[k][0] > 0 ? (height - i) : i + 1);
                    int lmax = Math.min(imin, jmin);
                    for (int t = 0; t < lmax; t++) {
                        int v = cells[i + t * d[k][0]][j + t * d[k][1]];
                        if (!rule.isNumberCell(v)) break;
                        sum += v;
                        if (sum >= rule.target) {
                            if (sum == rule.target) {
                                l = t;
                            }
                            break;
                        }
                    }
                    if (l > 0) {
                        for (int t = 0; t <= l; t++) {
                            ++ret[i + t * d[k][0]][j + t * d[k][1]];
                        }
                    }
                }
            }
        }
        return ret;
    }

    // マーク情報をもとに番号ブロックを消していくよ
    private int sweep(int[][] mark) {
        int ret = 0;
        int height = rule.getBoardHeight();
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < rule.width; ++j) {
                if (mark[i][j] > 0) {
                    ret += mark[i][j];
                    cells[i][j] = rule.empty;
                }
            }
        }
        return ret;
    }


    // 重力が働く。ブロックセルは下に移動する
    private int[] check() {
        int blocks = 0;
        int obstacles = 0;
        int height = rule.getBoardHeight();
        int highest = height;
        for (int j = 0; j < rule.width; j++) {
            int top = height;
            for (int i = 0; i < height; i++) {
                if (rule.isBlockCell(cells[i][j])) {
                    top = i;
                    break;
                }
            }
            for (int i = top; i < height; i++) {
                // num or obstacle
                if (rule.isObstacleCell(cells[i][j])) {
                    obstacles++;
                }
                blocks++;
            }
            highest = Math.min(highest, top );
        }
        return new int[]{blocks, obstacles, highest};
    }

    // 加点対象のセルパターンを数える
    private int potential() {
        int height = rule.getBoardHeight();
        int start = Math.max(rule.getBoardDeadLine(),2);
        int ret = 0;
        for (int i = start; i < height; ++i) {
            for (int j = 0; j < rule.width; ++j) {
                for(int k = -1;k<=1;k++){
                    int jj = j+k;
                    if(jj>=0&&jj<rule.width&&cells[i][j]+cells[i-2][jj] == rule.target)
                        ret++;
                }
            }
        }
        return ret;
    }




    // パックを落としたことによる発火や、その後の盤面の評価用のパラメータを収集するよ。ここをよくいじるよ。
    private Stats getBoardStats(boolean dead, int chainCount, int chainScore, int sweepCount, int[] lastSearch) {
        int height = rule.getBoardHeight();
        int deadline = rule.getBoardDeadLine();
        Stats s = new Stats(); // 変更が極めて激しいのでコンストラクタとか書いてらんない
        s.dead = dead; // そもそもパック置くのに失敗してたら死にます
        s.chain = chainCount;
        s.score = chainScore;
        s.damage = Specification.getObstacleFromScore(chainScore);
        s.sweeped = sweepCount;
        s.dead = lastSearch[2] < deadline; // lastSearch[2] is highest block
        s.blocks = lastSearch[0];
        s.obstacles = lastSearch[1];
        s.cells = height * rule.width;
        s.potential = potential();
        return s;
    }

    //破壊的なメソッドだよ。回転済みのパックと位置を指定して落とすよ
    public Stats commit(Pack pack, int x) {
        if (!put(pack, x)) {
            return getBoardStats(true, 0, 0, 0, new int[]{0, 0, 0});
        }
        fall();
        int chainCount = 0;
        int sweepCount;
        int totalSweepCount = 0;
        int chainScore = 0;
        do {
            int[][] counts = mark();
            sweepCount = sweep(counts);
            chainCount++;
            // スコアロジックが外部仕様なので外側に静的に持つ
            chainScore += Specification.getSingleSweepScore(chainCount, sweepCount);
            totalSweepCount += sweepCount;
            fall();
        } while (sweepCount > 0);
        int[] check = check();
        return getBoardStats(false, chainCount - 1, chainScore, totalSweepCount, check);
    }
}
