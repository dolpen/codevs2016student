package net.dolpen.codevs2016.models;

import net.dolpen.codevs2016.Specification;
import net.dolpen.codevs2016.utils.Array2d;

import java.util.Scanner;

/**
 * 盤面
 */
public class Pack {

    public int size;

    public int cells[][];

    public static Pack fromScanner(Scanner sc, Rule rule) {
        Pack pack = new Pack();
        pack.size = rule.packSize;
        pack.cells = Array2d.fromScanner(sc, pack.size, pack.size);
        return pack;
    }

    public static Pack[] arrayFromScanner(Scanner sc, Rule rule) {
        Pack[] packs = new Pack[rule.turns];
        for (int i = 0; i < rule.turns; ++i) packs[i] = fromScanner(sc, rule);
        return packs;
    }

    public Pack clone() {
        Pack pack = new Pack();
        pack.size = size;
        pack.cells = Array2d.clone(cells, size, size);
        return pack;
    }


    public Pack rotate(int rot) {
        Pack pack = new Pack();
        pack.size = size;
        pack.cells = Array2d.rotate(cells, size, rot);
        return pack;
    }

    public int fillObstaclePack(int obstacleId, int obstacles) {
        for (int i = 0; i < size; ++i) {
            for (int j = 0; j < size; ++j) {
                if (cells[i][j] == Specification.CELL_EMPTY && obstacles > 0) {
                    --obstacles;
                    cells[i][j] = obstacleId;
                    if (obstacles <= 0) return 0;
                }
            }
        }
        return obstacles;
    }

    public int getLeft() {
        for (int j = 0; j < size; ++j) {
            for (int i = 0; i < size; ++i) {
                if (cells[i][j] != Specification.CELL_EMPTY) return j;
            }
        }
        return size;
    }

    public int getRight() {
        for (int j = size - 1; j >= 0; --j) {
            for (int i = 0; i < size; ++i) {
                if (cells[i][j] != Specification.CELL_EMPTY) return j;
            }
        }
        return -1;
    }

    // left,right
    public int[] canFallXRange(int boardWidth) {
        return new int[]{-getLeft(), boardWidth - getRight() - 1};
    }


}
