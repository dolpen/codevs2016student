package net.dolpen.codevs2016.models;

import java.util.Scanner;

/**
 * 盤面
 */
public class Game {

    public Rule rule;

    public int now;

    public long time;

    // パック
    public Pack[] packs;

    public Board my;

    public Board op;

    public Scanner sc;

    public Game(Scanner sc) {
        rule = Rule.initFromScanner(sc);
        now = 0;
        packs = Pack.arrayFromScanner(sc, rule);
        this.sc = sc;
    }

    public void update() {
        now = sc.nextInt();
        time = sc.nextLong();
        int stock = sc.nextInt();
        my = Board.fromScanner(sc, rule, stock);
        stock = sc.nextInt();
        op = Board.fromScanner(sc, rule, stock);
    }

    public Pack nowPack() {
        return packs[now];
    }

    public Pack nextPack(int i) {
        return packs[now + i];
    }

}
