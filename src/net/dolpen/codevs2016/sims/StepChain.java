package net.dolpen.codevs2016.sims;

import net.dolpen.codevs2016.models.*;

import java.util.Comparator;

/**
 * "次の1手" と 数手先を検索中の最後の状況を保持する入れ物
 */
public class StepChain {

    public Step latest;

    public Step next;

    public Policy policy = Policy.NORMAL;

    public Stats bestStats = null;

    public int cachedScore = -100;


    private StepChain() {
    }


    public StepChain(StepChain chain, Step latest) {
        this.latest = latest;
        this.next = chain.next == null ? latest : chain.next;
        this.policy = chain.policy;
        this.bestStats = (chain.bestStats != null && chain.getScore() > latest.stats.getScore(policy)) ? chain.bestStats : latest.stats;
    }

    public Step getLatestStep() {
        return latest;
    }

    public Step getNextStep() {
        return next;
    }

    public int getScore() {
        if (cachedScore == -100) cachedScore = bestStats.getScore(policy);
        return cachedScore;
    }

    public static Comparator<StepChain> COMPARATOR = (a, b) -> {
        return b.getScore() - a.getScore();
    };

    public static StepChain none(Board bd) {
        StepChain ret = new StepChain();
        ret.latest = ret.next = Step.zero(bd, Behavior.none(null));
        ret.bestStats = ret.next.stats;
        return ret;
    }

    public static StepChain initial(Board bd, Policy policy) {
        StepChain ret = new StepChain();
        ret.latest = Step.zero(bd, Behavior.none(null));
        ret.policy = policy;
        return ret;
    }
}
