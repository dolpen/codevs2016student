package net.dolpen.codevs2016.sims;

import net.dolpen.codevs2016.models.*;

/**
 * 前状態+行動 -> 後状態の管理と評価値保存用
 */
public class Step {

    public Board before;

    public Behavior behavior;

    public Board after = null;

    public Stats stats;

    private Step() {

    }

    public Step(Board before, Behavior behavior) {
        this.before = before;
        this.behavior = behavior;
        this.after = this.before.clone();
        this.stats = this.after.commit(behavior.pack, behavior.x);
    }

    public static Step zero(Board board, Behavior none) {
        Step ret = new Step();
        ret.after = board;
        ret.behavior = none;
        ret.stats = new Stats();
        return ret;
    }

    public Behavior getBehavior() {
        return behavior;
    }
}
