package net.dolpen.codevs2016.sims;

import net.dolpen.codevs2016.models.Stats;

/**
 * AIの行動指針
 */
public enum Policy {
    FIRE {
        @Override
        public int evaluate(Stats s) {
            if (s.isDead()) return -1;
            if (!s.isFired()) return s.potential;
            return s.damage * 10000;
        }
    }, // 火力重視
    POTENTIAL {
        @Override
        public int evaluate(Stats s) {
            if (s.isDead()) return -1;
            boolean neededFire = s.isDangerous() || s.chain >= 15;
            if (s.isFired())
                return s.damage * (neededFire ? 10000 : 1);
            return s.potential;
        }
    }, // 盤面ポテンシャル
    NORMAL {
        @Override
        public int evaluate(Stats s) {
            if (s.isDead()) return -1;
            if (s.isFired()) {
                return s.damage * (s.isDangerous() ? 5 : 1);
            }
            return s.potential;
        }
    }; // 何もしない

    public abstract int evaluate(Stats s);
}
