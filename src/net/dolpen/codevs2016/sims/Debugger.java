package net.dolpen.codevs2016.sims;

import net.dolpen.codevs2016.models.*;

/**
 * デバッガ
 */
public class Debugger {

    private int score;

    private int maxChain;


    public Debugger() {
        score = 0;
        maxChain = 0;
    }

    public String exec(Game game, StepChain stepChain) {
        Board my = game.my.clone();
        Behavior behavior = stepChain.getNextStep().getBehavior();
        if (behavior.pack == null) return "";
        Stats s = my.commit(behavior.pack, behavior.x);
        if (s.score > 0) score += s.score;
        maxChain = Math.max(maxChain, s.chain);
        return String.format("%s,x = %d(%s)", s.toString(), stepChain.getScore(), stepChain.policy.name());
    }

    @Override
    public String toString() {
        return String.format("score : %d, chain : %d", score, maxChain);
    }
}
