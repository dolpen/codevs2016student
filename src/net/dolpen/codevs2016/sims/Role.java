package net.dolpen.codevs2016.sims;

/**
 * シミュレーションするロール
 */
public enum Role {
    ENEMY,
    PLAYER
}
