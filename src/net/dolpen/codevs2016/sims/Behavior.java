package net.dolpen.codevs2016.sims;


import net.dolpen.codevs2016.models.Pack;

/**
 * 行動
 */
public class Behavior {

    public int x;

    public int rotation;

    public Pack pack; // rotated!

    public Behavior(int x, int r, Pack pack) {
        this.x = x;
        this.rotation = r;
        this.pack = pack;
    }

    @Override
    public String toString() {
        return String.format("%d %d", x, rotation);
    }

    public static Behavior none(Pack pack) {
        return new Behavior(0, 0, pack);
    }
}
