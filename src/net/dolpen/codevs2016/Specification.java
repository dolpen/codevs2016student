package net.dolpen.codevs2016;

import net.dolpen.codevs2016.ai.*;
import net.dolpen.codevs2016.models.Game;

/**
 * ゲームの仕様管理
 */
public class Specification {

    public static String AI_NAME = "AIrim"; // mirIA

    public static int CELL_EMPTY = 0;

    public static int[] BEAM_WIDTH = new int[]{500, 500, 500, 200, 200, 200};

    // Brain選択
    public static Brain getBrain(Game g) {
        return new AdvancedBrain(g);
    }

    // 連鎖数と消去数から1チェイン分のスコアを出す
    public static int getSingleSweepScore(int chainCount, int sweepCount) {
        return (int) Math.floor(Math.pow(1.3, (double) chainCount)) * (sweepCount >> 1);
    }

    // 1コミットの総獲得スコアからお邪魔送信数を出す
    public static int getObstacleFromScore(int chainScore) {
        return chainScore / 5;
    }
}
