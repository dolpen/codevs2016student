package net.dolpen.codevs2016;


import net.dolpen.codevs2016.ai.*;
import net.dolpen.codevs2016.models.*;
import net.dolpen.codevs2016.sims.*;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        new Main().run();
    }

    void run() {
        println(Specification.AI_NAME);
        try (Scanner in = new Scanner(System.in)) {
            Game game = new Game(in);
            Brain brain = Specification.getBrain(game);
            Debugger debugger = new Debugger();
            while (true) {
                game.update();
                StepChain chain = brain.searchBestStepChain();
                println(chain.getNextStep().getBehavior().toString());
                debug("turn:" + game.now);
                debugger.exec(game, chain);
                debug(debugger.toString());
                debug(brain.bps()+"bps");
            }
        }
    }

    void println(String msg) {
        System.out.println(msg);
        System.out.flush();
    }

    void debug(String msg) {
        System.err.println(msg);
        System.err.flush();
    }
}
