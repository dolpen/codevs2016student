package net.dolpen.codevs2016.ai;

import net.dolpen.codevs2016.models.Game;
import net.dolpen.codevs2016.sims.*;

/**
 * 1st gen. 1人ゲームする時のAIで相手の盤面を見ない
 */
public class StandardBrain extends Brain {


    public StandardBrain(Game g) {
        super(g);
    }

    // brain core.
    @Override
    public StepChain solve() {
        return super.search(Role.PLAYER, Policy.NORMAL);
    }
}
