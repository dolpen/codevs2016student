package net.dolpen.codevs2016.ai;

import net.dolpen.codevs2016.Specification;
import net.dolpen.codevs2016.models.*;
import net.dolpen.codevs2016.sims.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 脳味噌
 */
public abstract class Brain {

    private Game game; // is NOT cloned instance

    // STOPWATCH!
    public int count;
    public long startTime;
    public long endTime;

    public Brain(Game g) {
        game = g;
    }

    private List<StepChain> getBeam(List<StepChain> chains, Pack nextPack, int length) {
        PriorityQueue<StepChain> q = new PriorityQueue<>(length, StepChain.COMPARATOR);
        for (StepChain chain : chains) {
            for (int r = 0; r < 4; r++) {
                Pack falling = nextPack.rotate(r);
                int[] lr = falling.canFallXRange(game.rule.width);
                for (int x = lr[0]; x <= lr[1]; x++) {
                    Step ns = new Step(
                        chain.getLatestStep().after,//.clone(),
                        new Behavior(x, r, falling)
                    );
                    count++;
                    if (!ns.stats.dead) {
                        q.offer(new StepChain(chain, ns));
                    }
                }
            }
        }
        return q.stream().limit(Math.min(q.size(), length)).collect(Collectors.toList());
    }

    protected StepChain search(Role role, Policy policy) {
        // initial board
        Board bd = role == Role.ENEMY ? game.op.clone() : game.my.clone();
        int stocked = bd.stocked;
        // tree root(before)
        List<StepChain> chain = new ArrayList<>();
        chain.add(StepChain.initial(bd, policy));
        // beam search(after i turns)
        for (int i = 0; i < Specification.BEAM_WIDTH.length; ++i) {
            if (game.now + i >= game.rule.turns) break;
            Pack nowPack = game.nextPack(i).clone();
            stocked = nowPack.fillObstaclePack(game.rule.obstacle, stocked);
            chain = getBeam(chain, nowPack, Specification.BEAM_WIDTH[i]);
        }
        // could not find best/any chain, do nothing (same as NoneAI)
        if (chain.isEmpty()) {
            return StepChain.none(bd);
        }
        // best step chain, contains best behavior in this turn
        return chain.get(0);
    }

    protected StepChain searchNext(Role role, Policy policy) {
        // initial board
        Board bd = role == Role.ENEMY ? game.op.clone() : game.my.clone();
        int stocked = bd.stocked;
        // tree root(before)
        List<StepChain> chain = new ArrayList<>();
        chain.add(StepChain.initial(bd, policy));
        // beam search(after i turns)
        if (game.now < game.rule.turns) {
            Pack nowPack = game.nextPack(0).clone();
            nowPack.fillObstaclePack(game.rule.obstacle, stocked);
            chain = getBeam(chain, nowPack, 1000);
        }
        // could not find best/any chain, do nothing (same as NoneAI)
        if (chain.isEmpty()) {
            return StepChain.none(bd);
        }
        // best step chain, contains best behavior in this turn
        return chain.get(0);
    }

    public long bps(){
        return (1000000000L*count)/(endTime-startTime); // board/sec
    }

    public double time(){
        return (double)(endTime-startTime)/1000000000L;
    }



    protected abstract StepChain solve();

    public StepChain searchBestStepChain(){
        startTime = System.nanoTime();
        count = 0;
        StepChain ans = solve();
        endTime = System.nanoTime();
        return ans;
    }
}
