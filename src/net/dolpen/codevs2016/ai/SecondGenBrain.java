package net.dolpen.codevs2016.ai;

import net.dolpen.codevs2016.models.Game;
import net.dolpen.codevs2016.sims.*;

/**
 * 潰し対応が早く、泥仕合になるとそれなりに強い
 */
public class SecondGenBrain extends Brain {

    public SecondGenBrain(Game g) {
        super(g);
    }

    public StepChain solve() {
        StepChain en = this.searchNext(Role.ENEMY, Policy.FIRE);
        if (en.bestStats.isFired()) {
            if (en.bestStats.damage >= 5) {
                StepChain me = this.searchNext(Role.PLAYER, Policy.FIRE);
                if (en.bestStats.damage <= me.bestStats.damage) {
                    return me;
                }
            }

            return this.search(Role.PLAYER, Policy.POTENTIAL);
        } else {
            return this.search(Role.PLAYER, Policy.NORMAL);
        }
    }
}
