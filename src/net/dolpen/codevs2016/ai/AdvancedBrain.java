package net.dolpen.codevs2016.ai;

import net.dolpen.codevs2016.models.Game;
import net.dolpen.codevs2016.sims.*;

/**
 * 最新のAI、ポテンシャルは高いが、2ndGenが潰し対応を早めているため有利が取りづらい
 */
public class AdvancedBrain extends Brain {

    public AdvancedBrain(Game g) {
        super(g);
    }

    // brain core.
    public StepChain solve() {
        // 相手の盤面凝視
        StepChain en = searchNext(Role.ENEMY, Policy.FIRE);
        if (en.bestStats.isFired()) {
            // 相手が近いうちにまとまった発火しそう(するとは言ってない)
            if (en.bestStats.damage >= 8) {
                // 潰せるなら潰す
                StepChain me = searchNext(Role.PLAYER, Policy.FIRE);
                if (en.bestStats.damage <= me.bestStats.damage) {
                    return me;
                }
            }
            return search(Role.PLAYER, Policy.POTENTIAL);
        }
        // 特に注意事項がないなら1人ゲームやる
        return search(Role.PLAYER, Policy.NORMAL);
    }
}
