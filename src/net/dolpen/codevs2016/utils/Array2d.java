package net.dolpen.codevs2016.utils;

import java.util.*;

/**
 * 様々なボイラープレート
 */
public class Array2d {

    public static int[][] clone(int[][] original, int width, int height) {
        int[][] copied = new int[height][width];
        for (int i = 0; i < height; ++i)
            copied[i] = Arrays.copyOf(original[i], width);
        return copied;
    }


    public static int[][] zero(int width, int height) {
        int[][] generated = new int[height][width];
        for (int i = 0; i < height; ++i)
            Arrays.fill(generated[i], 0);
        return generated;
    }

    public static int[][] fromScanner(Scanner sc, int width, int height) {
        int[][] loaded = new int[height][width];
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                loaded[i][j] = sc.nextInt();
            }
        }
        sc.next();
        return loaded;
    }

    public static int[][] offsetFromScanner(Scanner sc, int width, int height, int offsetHeight) {
        int[][] loaded = new int[height + offsetHeight][width];
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                loaded[i + offsetHeight][j] = sc.nextInt();
            }
        }
        sc.next();
        return loaded;
    }

    public static int[][] rotate(int[][] pack, int size, int rot) {
        for (int i = 0; i < rot; ++i) {
            pack = rotateSingle(pack, size);
        }
        return pack;
    }

    public static int[][] rotateSingle(int[][] pack, int size) {
        int[][] res = clone(pack, size, size);
        for (int i = 0; i < size; ++i) {
            for (int j = 0; j < size; ++j) {
                res[j][size - i - 1] = pack[i][j];
            }
        }
        return res;
    }

    public static String toString(int[][] original, int width, int height) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                sb.append(original[i][j]).append(",");
            }
            sb.append("\n");
        }
        return sb.toString();


    }
}
