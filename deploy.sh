#!/bin/bash

mkdir -p work
cd work
rm -f ./*
cp ../out/artifacts/codevs_jar/codevs.jar ./
cp ../launch.sh ./
zip deploy.zip ./codevs.jar ./launch.sh
